import 'package:sudoku_machine/sudoku_machine.dart' as sudoku_machine;

void main(List<String> arguments) {
  const width = 9;
  var puzzle = sudoku_machine.SudokuGeneratorRegular(width);
  print(puzzle.board);
  print("verified: ${puzzle.board.verify(strict: true)}");
}
