workflow:
  name: 'Build CI'

# https://hub.docker.com/_/dart
image: dart:latest

variables:
  # To learn more go to https://dart.dev/tools/dart-test
  # Or run `dart test --help`
  TEST_ARGS: >-
    --platform vm
    --timeout 30s
    --concurrency=6
    --test-randomize-ordering-seed=random
    --reporter=expanded
    --file-reporter=json:reports.json

.use-pub-cache-bin:
  # Define commands that need to be executed before each job.
  before_script:
    # Set PUB_CACHE either here or in the CI/CD Settings if you have multiple jobs that use dart commands.
    # PUB_CACHE is used by the `dart pub` command, it needs to be set so package dependencies are stored at the project-level for CI/CD operations.
    - export PUB_CACHE=".pub-cache"
    - export PATH="$PATH:$HOME/$PUB_CACHE/bin"

# Cache generated files and plugins between builds.
.upload-cache:
  cache:
    key: "$CI_COMMIT_SHA"
    when: 'on_success'
    paths:
      - .pub-cache/bin/
      - .pub-cache/global_packages/
      - .pub-cache/hosted/
      - .dart_tool/
      - .packages

# Cache downloaded dependencies and plugins between builds.
# To keep cache across branches add 'key: "$CI_JOB_NAME"'
.download-cache:
  cache:
    key: "$CI_COMMIT_SHA"
    paths:
      - .pub-cache/bin/
      - .pub-cache/global_packages/
      - .pub-cache/hosted/
      - .dart_tool/
      - .packages
    policy: pull

install-dependencies:
  stage: .pre
  extends:
    - .use-pub-cache-bin
    - .upload-cache
  script:
    - dart pub get --no-precompile

build:
  stage: build
  needs:
    - install-dependencies
  extends:
    - .use-pub-cache-bin
    - .upload-cache
  script:
    - dart pub get --offline --precompile

unit-test:
  stage: test
  needs:
    - build
  extends:
    - .use-pub-cache-bin
    - .download-cache
  before_script:
    - dart pub global activate coverage
    - dart pub global activate junitreport_maintained
  script:
    - dart pub global run coverage:test_with_coverage -- $TEST_ARGS
    - dart pub global run junitreport_maintained:tojunit --input reports.json --output test-report.xml
  after_script:
    - apt-get update
    - apt-get install -y curl gnupg coreutils
    # download Codecov CLI
    - curl -Os https://cli.codecov.io/latest/linux/codecov

    # integrity check
    - curl https://keybase.io/codecovsecurity/pgp_keys.asc | gpg --no-default-keyring --keyring trustedkeys.gpg --import # One-time step  
    - curl -Os https://cli.codecov.io/latest/linux/codecov
    - curl -Os https://cli.codecov.io/latest/linux/codecov.SHA256SUM
    - curl -Os https://cli.codecov.io/latest/linux/codecov.SHA256SUM.sig
    - gpgv codecov.SHA256SUM.sig codecov.SHA256SUM
    - shasum -a 256 -c codecov.SHA256SUM

    # upload report
    - chmod +x codecov
    - ./codecov upload-process --token $CODECOV_TOKEN
  artifacts:
    reports:
      junit: test-report.xml

lint-test:
  stage: test
  needs:
    - install-dependencies
  extends:
    - .use-pub-cache-bin
    - .download-cache
  script:
    - dart analyze .

format-test:
  stage: test
  needs:
    - install-dependencies
  extends:
    - .use-pub-cache-bin
    - .download-cache
  script:
    - dart format --set-exit-if-changed bin/ lib/ test/
