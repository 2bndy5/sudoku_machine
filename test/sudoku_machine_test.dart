import 'package:sudoku_machine/board.dart';
import 'package:sudoku_machine/grid.dart';
import 'package:sudoku_machine/solver.dart';
import 'package:sudoku_machine/sudoku_machine.dart';
import 'package:test/test.dart';

void main() {
  test('find grid', () {
    var board = SudokuBoardRegular(9);
    const row = 8;
    const col = 8;
    final grid = board.findGrid(row, col);
    expect(grid.cellCoordinates, board.grids[8].cellCoordinates);
    expect(board.grids[8].cellCoordinates[0].toString(), "(row: 6, col: 6)");
    expect(board.getGridIndex(row, col), 8);
  });

  for (final length in [4, 6, 9, 12, 16]) {
    test('board generator (regular ${length}x$length)', () {
      var puzzle = SudokuGeneratorRegular(length);
      expect(puzzle.board.verify(), true);
    });
  }

  test('board generator fail (bad length)', () {
    const length = 3;
    try {
      SudokuGeneratorRegular(length);
    } on SudokuUnsupportedGridSize catch (exc) {
      expect(exc.length, length);
      assert(
          exc.toString().startsWith("$length is not a supported board size "));
    }
  });

  test('board import parser fail', () {
    try {
      SudokuParserRegularString("111122223333....");
    } on SudokuUnsolvable catch (exc) {
      expect(exc.badBoard.length, 4);
      assert(exc
          .toString()
          .startsWith("Could not find a solution for the given board:"));
    }
  });

  var testImport = {0: "zero", 1: "dash", 2: "dot"};
  testImport.forEach((key, value) {
    test("test import from file (with $value)", () {
      var puzzle = SudokuParserRegularPath("test/test_import$key.txt");
      expect(puzzle.board.length, 9);
      expect(puzzle.solution.verify(), true);
    });
  });
}
