class SudokuCoordinate {
  final int row;
  final int col;

  SudokuCoordinate(this.row, this.col);

  @override
  String toString() {
    return "(row: $row, col: $col)";
  }
}

class SudokuGrid {
  List<SudokuCoordinate> cellCoordinates;

  SudokuGrid(this.cellCoordinates);

  SudokuGrid copy() {
    return SudokuGrid(List.from(cellCoordinates));
  }

  bool isMember(int row, int col) {
    for (final cell in cellCoordinates) {
      if (cell.row == row && cell.col == col) {
        return true;
      }
    }
    return false;
  }

  @override
  String toString() {
    final str = List.generate(
      cellCoordinates.length,
      (index) => cellCoordinates[index].toString(),
    ).join(", ");
    return "{$str}";
  }
}

class SudokuUnsupportedGridSize implements Exception {
  final int length;
  const SudokuUnsupportedGridSize(this.length);

  @override
  String toString() {
    return "$length is not a supported board size using regular grids.";
  }
}

mixin SudokuBoardGridsRegular {
  List<SudokuGrid> grids = [];
  int width = 0;
  int height = 0;

  void makeGrids(int length) {
    switch (length) {
      case 16:
        width = 4;
        height = 4;
      case 12:
        width = 4;
        height = 3;
      case 9:
        width = 3;
        height = 3;
      case 6:
        width = 3;
        height = 2;
      case 4:
        width = 2;
        height = 2;
      default:
        throw SudokuUnsupportedGridSize(length);
    }

    List<List<SudokuCoordinate>> temp = List.generate(length, (index) => []);
    for (var row = 0; row < length; row++) {
      for (var col = 0; col < length; col++) {
        var gridRow = row ~/ height;
        var gridCol = col ~/ width;
        var index = gridRow * height + gridCol;
        // print("cell[$row][$col] is going in grid ($gridRow, $gridCol = $index)");
        temp[index].add(SudokuCoordinate(row, col));
      }
    }
    for (var grid in temp) {
      grids.add(SudokuGrid(grid));
    }
  }

  int getGridIndex(int row, int col) {
    for (var (index, grid) in grids.indexed) {
      if (grid.isMember(row, col)) {
        return index;
      }
    }
    throw Exception("grid corresponding to cell[$row][$col] not found");
  }
}

bool _isCellInGrids(int row, int col, List<SudokuGrid> grids) {
  for (var grid = 0; grid < grids.length; grid++) {
    if (grids[grid].isMember(row, col)) {
      // print("cell (row: $row, col: $col) is already a part of grid[$grid]");
      return true;
    }
  }
  return false;
}

List<SudokuCoordinate> _rankPossibles(
  SudokuGrid newGrid,
  int length,
  List<SudokuGrid> grids,
  List<SudokuCoordinate> possibles,
  int rank,
) {
  var result = <SudokuCoordinate>[];
  for (var coord in possibles) {
    var sides = [
      SudokuCoordinate(coord.row - 1, coord.col), // up
      SudokuCoordinate(coord.row + 1, coord.col), // down
      SudokuCoordinate(coord.row, coord.col - 1), // left
      SudokuCoordinate(coord.row, coord.col + 1), // right
    ];
    var occupiedSides = 0;
    for (var c in sides) {
      if (c.row < 0 || c.col < 0 || c.row >= length || c.col >= length) {
        occupiedSides++;
      } else if (_isCellInGrids(c.row, c.col, grids) ||
          newGrid.isMember(c.row, c.col)) {
        occupiedSides++;
      }
    }
    if (occupiedSides > rank) {
      result.add(coord);
    }
  }
  return result;
}

List<SudokuCoordinate> _getNeighbors(
  SudokuGrid newGrid,
  int length,
  List<SudokuGrid> grids,
) {
  // print("finding neighbors for cells $newGrid");
  if (newGrid.cellCoordinates.isEmpty) {
    return [SudokuCoordinate(0, 0)];
  }
  var possibles = <SudokuCoordinate>[];
  for (var coord in newGrid.cellCoordinates) {
    var sides = [
      SudokuCoordinate(coord.row - 1, coord.col), // up
      SudokuCoordinate(coord.row + 1, coord.col), // down
      SudokuCoordinate(coord.row, coord.col - 1), // left
      SudokuCoordinate(coord.row, coord.col + 1), // right
    ];
    for (var c in sides) {
      if (c.row < 0 || c.col < 0 || c.row >= length || c.col >= length) {
        continue;
      } else if (!_isCellInGrids(c.row, c.col, grids) &&
          !newGrid.isMember(c.row, c.col)) {
        // print("cell $c is a possible neighbor");
        possibles.add(c);
      }
    }
  }
  // print("neighbors found (${possibles.length}): $possibles");
  // rank the cells based on possible occupants
  var result = <SudokuCoordinate>[];
  var rank = 3;
  while (rank >= 1) {
    result = _rankPossibles(newGrid, length, grids, possibles, rank);
    if (result.isNotEmpty) {
      break;
    }
    rank--;
  }
  // print("neighbors with $rank or more neighbors: (${result.length}): $result");
  possibles.shuffle();
  if (result.isEmpty && possibles.isNotEmpty) {
    return possibles;
  }
  return result;
}

List<SudokuCoordinate> _findUnclaimedCell(int length, List<SudokuGrid> grids) {
  if (grids.isEmpty) {
    return [SudokuCoordinate(0, 0)];
  }
  var result = <SudokuCoordinate>[];
  for (var row = 0; row < length; row++) {
    for (var col = 0; col < length; col++) {
      if (!_isCellInGrids(row, col, grids)) {
        var cell = SudokuCoordinate(row, col);
        // print("found unclaimed cell at $cell");
        result.add(cell);
      }
    }
  }
  return result;
}

List<SudokuGrid>? resolveGrid(
  List<SudokuGrid> grids,
  int length,
  SudokuGrid newGrid,
) {
  // printGrids(length, grids, newGrid);
  if (grids.length == length) {
    print("cells allocated to grids");
    return grids; // base case
  }
  if (newGrid.cellCoordinates.isEmpty) {
    // var neighbors = _getNeighbors(grids.isEmpty ? newGrid : grids.last, length, grids);
    // if (neighbors.isEmpty) {
    //   print("no unclaimed cells found");
    //   return null; // handle error in caller
    // }
    // for (var cell in neighbors) {
    //   var tmpGrid = SudokuGrid([cell]);
    //   var result = resolveGrid(grids, length, tmpGrid);
    //   if (result != null) {
    //     return result;
    //   }
    // }
    // return null;

    var cells = _findUnclaimedCell(length, grids);
    if (cells.isEmpty) {
      return null; // handle error in caller
    }
    // newGrid.cellCoordinates.add(cells);
    for (var cell in cells) {
      var tmpGrid = SudokuGrid([cell]);
      var result = resolveGrid(grids, length, tmpGrid);
      if (result != null) {
        return result;
      }
    }
    return null;
  }
  // print("newGrid.length: ${newGrid.cellCoordinates.length}: $newGrid");
  if (newGrid.cellCoordinates.length < length) {
    // find possible cells surrounding current grid
    var possibles = _getNeighbors(newGrid, length, grids);
    if (possibles.isEmpty) {
      // need to backtrack
      return null;
    }
    for (var possible in possibles) {
      // print("trying cell $possible in grid ${grids.length}");
      var cpNewGrid = newGrid.copy();
      cpNewGrid.cellCoordinates.add(possible);
      var result = resolveGrid(grids, length, cpNewGrid);
      if (result != null) {
        return result;
      }
    }
    // need to backtrack
    // print("possible neighbors in grid ${grids.length} didn't work out");
    grids.removeLast();
    return resolveGrid(grids, length, SudokuGrid([]));
  }
  var newGrids = List.generate(grids.length, (index) => grids[index].copy());
  newGrids.add(newGrid);
  return resolveGrid(newGrids, length, SudokuGrid([]));
}

void printGrids(int length, List<SudokuGrid> grids, SudokuGrid newGrid) {
  // debug printing
  final padding = length.toString().length + 1;
  for (var row = 0; row < length; row++) {
    var rowStr = "";
    for (var col = 0; col < length; col++) {
      int? grid;
      for (var g = 0; g < grids.length; g++) {
        if (grids[g].isMember(row, col)) {
          grid = g;
        }
      }
      if (newGrid.isMember(row, col)) {
        grid = grids.length;
      }
      if (grid != null) {
        rowStr += grid.toString().padLeft(padding);
      } else {
        rowStr += ".".padLeft(padding);
      }
    }
    print(rowStr);
  }
  print("");
}

mixin SudokuBoardGridsIrregular {
  List<SudokuGrid> grids = [];

  void makeGrids(int length) {
    var result = resolveGrid(grids, length, SudokuGrid([]));
    if (result == null) {
      throw Exception("failed to allocate cells to irregular grids");
    }
    grids = result;
  }

  int getGridIndex(int row, int col) {
    for (var (index, grid) in grids.indexed) {
      if (grid.isMember(row, col)) {
        return index;
      }
    }
    throw Exception("grid corresponding to cell[$row][$col] not found");
  }
}
