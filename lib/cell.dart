class SudokuCell {
  List<int> notes = [];
  int value = 0;

  SudokuCell([int? max]) {
    if (max != null) {
      notes = List.generate(max, (index) => index + 1);
    }
  }

  factory SudokuCell.from(SudokuCell cell) {
    var copy = SudokuCell(cell.notes.length);
    copy.notes = List.from(cell.notes);
    copy.value = cell.value;
    return copy;
  }

  @override
  String toString() {
    return value.toString();
  }
}
