import 'dart:io';
import 'dart:math';

import 'package:sudoku_machine/board.dart';
import 'package:sudoku_machine/solver.dart';

class SudokuGeneratorRegular {
  late SudokuBoardRegular board;
  SudokuGeneratorRegular(int length) {
    // populate a the first grid's cells as a seed
    var seed = SudokuBoardRegular(length, true);
    var gridSeed = List.generate(length, (index) => index + 1);
    gridSeed.shuffle();
    var grid0 = seed.grids[0].cellCoordinates;
    assert(length == seed.grids[0].cellCoordinates.length);
    for (var i = 0; i < gridSeed.length; i++) {
      seed.updateCell(grid0[i].row, grid0[i].col, gridSeed[i]);
    }

    // now fill in the rest
    var result = solve(seed) as SudokuBoardRegular?;
    if (result == null) {
      throw SudokuUnsolvable(seed); // coverage:ignore-line
    }
    board = result;
  }
}

class SudokuParserRegularString {
  late SudokuBoardRegular board;
  late SudokuBoardRegular solution;
  SudokuParserRegularString(String input) {
    input = input.trim();
    var length = sqrt(input.length).floor();
    board = SudokuBoardRegular(length, true);
    var index = 0;
    for (var row = 0; row < length; row++) {
      index = row * length;
      for (var col = 0; col < length; col++) {
        var char = input.codeUnitAt(index + col);
        if (char > 48 && char < 58) {
          board.matrix[row][col].value = char - 48;
        }
      }
    }
    var result = solve(board) as SudokuBoardRegular?;
    if (result == null) {
      throw SudokuUnsolvable(board);
    }
    solution = result;
  }
}

class SudokuParserRegularPath extends SudokuParserRegularString {
  SudokuParserRegularPath(String path) : super(File(path).readAsStringSync());
}

class SudokuGeneratorIrregular {
  late SudokuBoardIrregular board;
  SudokuGeneratorIrregular(int length) {
    // populate a the first grid's cells as a seed
    var seed = SudokuBoardIrregular(length, true);
    seed.makeGrids(length);
    var gridSeed = List.generate(length, (index) => index + 1);
    gridSeed.shuffle();
    var grid0 = seed.grids[0].cellCoordinates;
    assert(length == seed.grids[0].cellCoordinates.length);
    for (var i = 0; i < gridSeed.length; i++) {
      seed.updateCell(grid0[i].row, grid0[i].col, gridSeed[i]);
    }

    // now fill in the rest
    var result = solve(seed) as SudokuBoardIrregular?;
    if (result == null) {
      throw SudokuUnsolvable(seed); // coverage:ignore-line
    }
    board = result;
  }
}
