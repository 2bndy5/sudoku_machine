import 'package:sudoku_machine/cell.dart';
import 'package:sudoku_machine/grid.dart';

abstract class SudokuBoard {
  List<List<SudokuCell>> matrix = [];
  final int length;

  SudokuBoard(this.length, [bool autoNotes = false]) {
    matrix = List.generate(
      length,
      (index) => List.generate(
        length,
        (index) => SudokuCell(autoNotes ? length : null),
      ),
    );
  }

  SudokuBoard copy();

  /// Sets a given [number] from the cell specified by [row] and [col].
  /// Also pops the given [number] from all cells' notes in the given [col] and [row]
  /// and cells in the corresponding grid.
  void updateCell(int row, int col, int number) {
    final grid = findGrid(row, col);
    for (var cell in grid.cellCoordinates) {
      matrix[cell.row][cell.col].notes.remove(number);
    }
    for (var i = 0; i < length; i++) {
      matrix[row][i].notes.remove(number);
    }
    for (var i = 0; i < length; i++) {
      matrix[i][col].notes.remove(number);
    }
    matrix[row][col].value = number;
    // skip this cell when returning list in findLeastNotes()
    matrix[row][col].notes.clear();
  }

  void updateNotes() {
    for (var row = 0; row < length; row++) {
      for (var col = 0; col < length; col++) {
        if (matrix[row][col].value != 0) {
          updateCell(row, col, matrix[row][col].value);
        }
      }
    }
  }

  /// Find a cells with the least amount of notes populated.
  List<SudokuCoordinate> findLeastNotes() {
    List<SudokuCoordinate> cells = [];
    var leastCount = length;
    for (var row = 0; row < length; row++) {
      for (var col = 0; col < length; col++) {
        var noteCount = matrix[row][col].notes.length;
        if (matrix[row][col].value <= 0) {
          if (noteCount < leastCount) {
            cells.insert(0, SudokuCoordinate(row, col));
            leastCount = noteCount;
          } else {
            cells.add(SudokuCoordinate(row, col));
          }
        }
      }
    }
    // print("cells w/ notes found: $cells");
    return cells;
  }

  bool isFilled() {
    for (var row = 0; row < length; row++) {
      for (var col = 0; col < length; col++) {
        if (matrix[row][col].value == 0) {
          return false;
        }
      }
    }
    return true;
  }

  List<int> isInColumn(int col, int number) {
    var dupes = <int>[];
    for (var i = 0; i < length; i++) {
      if (matrix[i][col].value == number) {
        dupes.add(i);
      }
    }
    return dupes;
  }

  List<int> isInRow(int row, int number) {
    var dupes = <int>[];
    for (var i = 0; i < length; i++) {
      if (matrix[row][i].value == number) {
        dupes.add(i);
      }
    }
    return dupes;
  }

  SudokuGrid findGrid(int row, int col);
  int getGridIndex(int row, int col);

  int isInGrid(int row, int col, int number) {
    var dupes = <int>[];
    final grid = findGrid(row, col);
    // print("grid cells: ${grid.cellCoordinates}");
    for (var cell in grid.cellCoordinates) {
      var value = matrix[cell.row][cell.col].value;
      if (value == number) {
        dupes.add(value);
      }
    }
    return dupes.length;
  }

  bool verify({bool strict = false}) {
    for (var row = 0; row < length; row++) {
      for (var col = 0; col < length; col++) {
        var number = matrix[row][col].value;
        if (number == 0) {
          if (strict) {
            print("col $col row $row is 0 (not set)");
            return false;
          } else {
            continue;
          }
        }
        var inCol = isInColumn(col, number);
        if (inCol.length > 1) {
          print("number $number is duplicated in col $col at indexes $inCol");
          return false;
        }
        var inRow = isInRow(row, number);
        if (inRow.length > 1) {
          print(
              "number $number is duplicated in row $row is duplicated at indexes $inRow");
          return false;
        }
        var gridDupes = isInGrid(row, col, number);
        if (gridDupes > 1) {
          final grid = findGrid(row, col);
          final startingCell = grid.cellCoordinates[0];
          print(
            "number $number is duplicated $gridDupes times in grid starting @ [${startingCell.row}][${startingCell.col}]",
          );
          return false;
        }
      }
    }
    return true;
  }
}

class SudokuBoardRegular extends SudokuBoard with SudokuBoardGridsRegular {
  SudokuBoardRegular(int length, [bool autoNotes = false])
      : super(length, autoNotes) {
    makeGrids(length);
  }

  @override
  SudokuBoardRegular copy() {
    var copy = SudokuBoardRegular(length);
    for (var i = 0; i < length; i++) {
      for (var j = 0; j < length; j++) {
        copy.matrix[i][j] = SudokuCell.from(matrix[i][j]);
      }
    }
    return copy;
  }

  @override
  SudokuGrid findGrid(int row, int col) {
    for (var grid in grids) {
      if (grid.isMember(row, col)) {
        return grid;
      }
    }
    throw Exception("grid corresponding to cell[$row][$col] not found");
  }

  @override
  String toString() {
    final padLength = 1 + length.toString().length;
    var longLine = "";
    for (var i = 0; i < length; i++) {
      var row = "\n";
      for (var j = 0; j < length; j++) {
        row += (matrix[i][j].value == 0 ? "-" : matrix[i][j].value.toString())
            .padRight(padLength);
        if ((j + 1) % width == 0 && j != length - 1) {
          row += "| ";
        }
      }
      longLine += row;
      if ((i + 1) % height == 0 && i != length - 1) {
        longLine += "\n";
        var div = List.generate(
            length ~/ width, (index) => "".padRight(width * padLength, "-"));
        div.last = div.last.substring(0, div.last.length - 1);
        longLine += div.join("+-");
      }
    }
    return longLine.trim();
  }
}

class SudokuBoardIrregular extends SudokuBoard with SudokuBoardGridsIrregular {
  SudokuBoardIrregular(int length,
      [bool autoNotes = false, bool randomizeGrids = false])
      : super(length, autoNotes) {
    if (randomizeGrids) {
      makeGrids(length);
    }
  }

  @override
  SudokuBoardIrregular copy() {
    var cp = SudokuBoardIrregular(length);
    for (var i = 0; i < length; i++) {
      for (var j = 0; j < length; j++) {
        cp.matrix[i][j] = SudokuCell.from(matrix[i][j]);
      }
    }
    cp.grids = grids;
    return cp;
  }

  @override
  SudokuGrid findGrid(int row, int col) {
    for (var grid in grids) {
      if (grid.isMember(row, col)) {
        return grid;
      }
    }
    throw Exception("grid corresponding to cell[$row][$col] not found");
  }

  @override
  String toString() {
    final padLength = 3 + length.toString().length;
    var longLine = "";
    for (var i = 0; i < length; i++) {
      var row = "\n";
      for (var j = 0; j < length; j++) {
        // find grid index
        int grid = 0;
        for (var g = 0; g < length; g++) {
          if (grids[g].isMember(i, j)) {
            grid = g;
            break;
          }
        }
        var gridIndex = "abcdefghijklmnop"[grid];
        row += "$gridIndex-${matrix[i][j].value}".padRight(padLength);
      }
      longLine += row;
    }
    return longLine.trim();
  }
}
