import 'package:sudoku_machine/board.dart';

class SudokuUnsolvable implements Exception {
  final SudokuBoard badBoard;
  const SudokuUnsolvable(this.badBoard);

  @override
  String toString() {
    return "Could not find a solution for the given board:\n$badBoard";
  }
}

SudokuBoard? solve(
  SudokuBoard board, [
  int? row,
  int? col,
  int? number,
]) {
  if (number != null && col != null && row != null) {
    if (board.isInColumn(col, number).isNotEmpty) {
      // print(
      //   "!!!!!! number $number is in col $col",
      // );
      return null;
    }
    if (board.isInRow(row, number).isNotEmpty) {
      // print(
      //   "!!!!!! number $number is in row $row",
      // );
      return null;
    }
    if (board.isInGrid(row, col, number) > 0) {
      // print(
      //     "!!!!!! number $number is in grid containing cell[$row][$col]");
      // print(board);
      return null;
    }
    board.updateCell(row, col, number);
  } else {
    // update notes based on current values set
    board.updateNotes();
  }
  // print(board);
  var cells = board.findLeastNotes();
  if (cells.isEmpty) {
    if (board.isFilled()) {
      // no cells left to fill in
      // print("-- board is filled");
      return board;
    }
    // print("!!! no cells w/ any notes and board is incomplete");
    // need to backtrack
    return null;
  }
  // print("cells[${cells.length}]: $cells");
  for (var cell in cells) {
    if (board.matrix[cell.row][cell.col].notes.isEmpty) {
      // print("!!!!!! cell $cell notes is depleted of possibilities\n$board");
      return null;
    }
    // print("cell $cell notes: ${board.solution[cell.row][cell.col].notes}");
    for (var note in board.matrix[cell.row][cell.col].notes) {
      // print("-- setting cell $cell to $note");
      var result = solve(
        board.copy(),
        cell.row,
        cell.col,
        note,
      );
      if (result != null) {
        return result;
      }
    }
    // need to backtrack if we get here
    // print("!!!! backtracking since no value for cell $cell worked.");
    return null;
  }
  // print("!!! backtracking from ${cells.length} cells remaining");
  return null;
}
