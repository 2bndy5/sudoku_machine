
[![pipeline status](https://gitlab.com/2bndy5/sudoku_machine/badges/main/pipeline.svg)](https://gitlab.com/2bndy5/sudoku_machine/-/commits/main)
[![codecov](https://codecov.io/gl/2bndy5/sudoku_machine/graph/badge.svg?token=JB2QSKN70E)](https://codecov.io/gl/2bndy5/sudoku_machine)

# Sudoku Machine
Yet another Sudoku board generator/solver written in dart.

## Features

- [x] regular grids
- [ ] irregular grids
- [x] variously sized matrix:
   - 4x4
   - 6x6
   - 9x9
   - 12x12
   - 16x16
